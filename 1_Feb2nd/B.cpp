//
// Created by Dusker on 2024/2/2.
//
#include <bits/stdc++.h>
#define ioclear std::ios::sync_with_stdio(false);std::cin.tie(nullptr);std::cout.tie(nullptr);
#define endl '\n'

void solve()
{
    using pii = std::pair<int, int>;
    using i64 = long long;
#define all(x) (x).begin(), (x).end()
    int n, ans = 0;
    std::cin >> n;
    std::vector<i64> fire(n);
    std::set<i64> s;
    auto getid = [&](int x, int y)
    {
        if(x == 1)
            return x * 1E9 + y;
        else
            return (x + 1) * 1E9 + y;
    };
    for(int i = 0;i < n;i++)
    {
        int x, y;
        std::cin >> x >> y;
        fire[i] = getid(x, y);
        s.insert(getid(x, y));
    }
    std::sort(all(fire));
    int left_cnt = 0, right_cnt = 0, left = 0, right = 0;
    for(int i = 0;i < n;i++)
    {
        int x = fire[i] / 1E9, y = fire[i] % (i64)1E9;
        if(x == 0 || x == 2)
            x++, y -= (i64)1E9;
//         std::cout << x << ' ' << y << endl;
        if(y < 0)
            left_cnt++;
        else
            right_cnt++;
        if(y == 0)
            continue;
        if(x == 1)
        {
            if(s.count(getid(2, y)) || s.count(getid(2, y - 1)) || s.count(getid(2, y + 1)))
            {
                if(y < 0)
                    left = 1;
                else
                    right = 1;
            }
        }
        else
        {
            if(s.count(getid(1, y)) || s.count(getid(1, y - 1)) || s.count(getid(1, y + 1)))
            {
                if(y < 0)
                    left = 1;
                else
                    right = 1;
            }
        }
    }
    if(left && right)
        ans = 0;
    else
    {
        if(left)
        {
            ans = right_cnt ? 1 : 2;
            ans = std::min(ans, 2 - (s.count(getid(2, 0)) ? 1 : 0) - (s.count(getid(1, 1)) ? 1 : 0));
        }
        else if(right)
        {
            ans = left_cnt ? 1 : 2;
            ans = std::min(ans, 2 - (s.count(getid(2, 0)) ? 1 : 0) - (s.count(getid(1, -1)) ? 1 : 0));
        }
        else
            ans = (left_cnt ? 1 : 2) + (right_cnt ? 1 : 2);
    }
    ans = std::min(ans, 3 - (s.count(getid(1, -1)) ? 1 : 0) - (s.count(getid(1, 1)) ? 1 : 0) - (s.count(getid(2, 0)) ? 1 : 0));
    std::cout << ans << endl;
}

int main()
{
#ifdef ONLINE_JUDGE
    ioclear;
#endif

    int t;
    std::cin >> t;
    while(t--)
        solve();
}