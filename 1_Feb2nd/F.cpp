//
// Created by Dusker on 2024/2/2.
//
#include <bits/stdc++.h>
#define ioclear std::ios::sync_with_stdio(false);std::cin.tie(nullptr);std::cout.tie(nullptr);
#define endl '\n'

constexpr int P = 1E9 + 7;
using i64 = long long;

// assume -P <= x < 2P
int norm(int x) {
    if (x < 0) {
        x += P;
    }
    if (x >= P) {
        x -= P;
    }
    return x;
}
template<class T>
T power(T a, i64 b) {
    T res = 1;
    for (; b; b /= 2, a *= a) {
        if (b % 2) {
            res *= a;
        }
    }
    return res;
}
struct Z {
    int x;
    Z(int x = 0) : x(norm(x)) {}
    Z(i64 x) : x(norm(x % P)) {}
    int val() const {
        return x;
    }
    Z operator-() const {
        return Z(norm(P - x));
    }
    Z inv() const {
        assert(x != 0);
        return power(*this, P - 2);
    }
    Z &operator*=(const Z &rhs) {
        x = i64(x) * rhs.x % P;
        return *this;
    }
    Z &operator+=(const Z &rhs) {
        x = norm(x + rhs.x);
        return *this;
    }
    Z &operator-=(const Z &rhs) {
        x = norm(x - rhs.x);
        return *this;
    }
    Z &operator/=(const Z &rhs) {
        return *this *= rhs.inv();
    }
    friend Z operator*(const Z &lhs, const Z &rhs) {
        Z res = lhs;
        res *= rhs;
        return res;
    }
    friend Z operator+(const Z &lhs, const Z &rhs) {
        Z res = lhs;
        res += rhs;
        return res;
    }
    friend Z operator-(const Z &lhs, const Z &rhs) {
        Z res = lhs;
        res -= rhs;
        return res;
    }
    friend Z operator/(const Z &lhs, const Z &rhs) {
        Z res = lhs;
        res /= rhs;
        return res;
    }
    friend std::istream &operator>>(std::istream &is, Z &a) {
        i64 v;
        is >> v;
        a = Z(v);
        return is;
    }
    friend std::ostream &operator<<(std::ostream &os, const Z &a) {
        return os << a.val();
    }
};

void solve()
{
    int n, m;
    std::cin >> n >> m;
    if(n < m)
    {
        std::cout << 0 << endl;
        return;
    }
    // S(n, m) = sum_(i=0)^m (-1)^(m-i) * i^n / (i! * (m - i)!)
    std::vector<Z> fac(m + 1), pi(m + 1);
    fac[0] = 1;
    for(int i = 1;i <= m;i++)
    {
        fac[i] = fac[i - 1] * i;
        pi[i] = power(Z(i), n);
    }
    Z ans = 0;
    for(int i = 0;i <= m;i++)
    {
        Z cur = pi[i] / (fac[i] * fac[m - i]);
        if((m - i) & 1)
            ans -= cur;
        else
            ans += cur;
    }
    std::cout << ans << endl;
}

int main()
{
#ifdef ONLINE_JUDGE
    ioclear;
#endif

    int t = 1;
//    std::cin >> t;
    while(t--)
        solve();
}