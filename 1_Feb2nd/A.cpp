#include <bits/stdc++.h>
#define ioclear std::ios::sync_with_stdio(false);std::cin.tie(nullptr);std::cout.tie(nullptr);
#define endl '\n'

void solve()
{
    int n, cnt1 = 0, cnt2 = 0;
    std::cin >> n;
    std::string s;
    std::cin >> s;
    for(int i = 0;i < n;i++)
        for(int j = i + 1;j < n;j++)
            for(int k = j + 1;k < n;k++)
            {
                if(s[i] == 'D' && s[j] == 'F' && s[k] == 'S')
                    cnt1 = 1;
                if(s[i] == 'd' && s[j] == 'f' && s[k] == 's')
                    cnt2 = 1;
            }
    std::cout << cnt1 << ' ' << cnt2 << endl;
}

int main()
{
#ifdef ONLINE_JUDGE
    ioclear;
#endif

    int t;
    std::cin >> t;
    while(t--)
        solve();
}