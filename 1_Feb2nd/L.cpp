//
// Created by Dusker on 2024/2/2.
//
#include <bits/stdc++.h>
#define ioclear std::ios::sync_with_stdio(false);std::cin.tie(nullptr);std::cout.tie(nullptr);
#define endl '\n'
#define int long long

void solve()
{
    using pii = std::pair<int, int>;
    using i64 = long long;
#define all(x) (x).begin(), (x).end()
    double c, d, h, w;
    std::cin >> c >> d >> h >> w;
    std::cout << std::fixed << std::setprecision(10);
    std::cout << 3.0 * c * w << endl;
}

signed main()
{
#ifdef ONLINE_JUDGE
    ioclear;
#endif

    int t;
    std::cin >> t;
    while(t--)
        solve();
}