//
// Created by Dusker on 2024/2/2.
//
#include <bits/stdc++.h>
#define ioclear std::ios::sync_with_stdio(false);std::cin.tie(nullptr);std::cout.tie(nullptr);
#define endl '\n'

constexpr int P = 998244353;
using i64 = long long;

// assume -P <= x < 2P
int norm(int x) {
    if (x < 0) {
        x += P;
    }
    if (x >= P) {
        x -= P;
    }
    return x;
}
template<class T>
T power(T a, i64 b) {
    T res = 1;
    for (; b; b /= 2, a *= a) {
        if (b % 2) {
            res *= a;
        }
    }
    return res;
}
struct Z {
    int x;
    Z(int x = 0) : x(norm(x)) {}
    Z(i64 x) : x(norm(x % P)) {}
    int val() const {
        return x;
    }
    Z operator-() const {
        return Z(norm(P - x));
    }
    Z inv() const {
        assert(x != 0);
        return power(*this, P - 2);
    }
    Z &operator*=(const Z &rhs) {
        x = i64(x) * rhs.x % P;
        return *this;
    }
    Z &operator+=(const Z &rhs) {
        x = norm(x + rhs.x);
        return *this;
    }
    Z &operator-=(const Z &rhs) {
        x = norm(x - rhs.x);
        return *this;
    }
    Z &operator/=(const Z &rhs) {
        return *this *= rhs.inv();
    }
    friend Z operator*(const Z &lhs, const Z &rhs) {
        Z res = lhs;
        res *= rhs;
        return res;
    }
    friend Z operator+(const Z &lhs, const Z &rhs) {
        Z res = lhs;
        res += rhs;
        return res;
    }
    friend Z operator-(const Z &lhs, const Z &rhs) {
        Z res = lhs;
        res -= rhs;
        return res;
    }
    friend Z operator/(const Z &lhs, const Z &rhs) {
        Z res = lhs;
        res /= rhs;
        return res;
    }
    friend std::istream &operator>>(std::istream &is, Z &a) {
        i64 v;
        is >> v;
        a = Z(v);
        return is;
    }
    friend std::ostream &operator<<(std::ostream &os, const Z &a) {
        return os << a.val();
    }
};

void solve()
{
    int n;
    Z ans = 1;
    std::cin >> n;
    std::vector<int> a(n + 1), vis(n + 1, -1);
    std::vector<std::string> s(n + 1);
    for(int i = 1;i <= n;i++)
        std::cin >> a[i] >> s[i];
    for(int i = 1;i <= n;i++)
    {
        int j = i;
        while(vis[j] == -1)
        {
            vis[j] = i;
            j = a[j];
        }
        if(vis[j] == i)
        {
            int k = j;
            std::vector<int> cycle;
            do
            {
                cycle.push_back(k);
                k = a[k];
            }while(k != j);
            int len = static_cast<int>(cycle.size());
            Z cur = 0;
            for(int x = 0;x < 5;x++)
            {
                int v = x;
                for(auto nxt: cycle)
                    v = s[nxt][v] - 'A';
                cur += v == x;
            }
            ans *= cur;
        }
    }
    std::cout << ans << endl;
}

int main()
{
#ifdef ONLINE_JUDGE
    ioclear;
#endif

    int t = 1;
//    std::cin >> t;
    while(t--)
        solve();
}