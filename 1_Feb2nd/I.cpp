//
// Created by Dusker on 2024/2/2.
//
#include <bits/stdc++.h>
#define ioclear std::ios::sync_with_stdio(false);std::cin.tie(nullptr);std::cout.tie(nullptr);
#define endl '\n'
#define int long long

void solve()
{
//    using pii = std::pair<int, int>;
//    using i64 = long long;
//#define all(x) (x).begin(), (x).end()
//    double c, d, h, w;
//    std::cin >> c >> d >> h >> w;
//    std::cout << std::fixed << std::setprecision(10) << (c * h + d * w) / (h + w) << endl;
//    std::cout << 3.0 * c * w << endl;
//    freopen("bit-noob.txt", "r", stdin);
    int n, sum = 0, xx = 0, yy = 0;
    std::cin >> n;
    std::vector<int> x(n + 1), y(n + 1);
    for(int i = 1;i <= n;i++)
    {
        int r;
        std::cin >> x[i] >> y[i] >> r;
        xx += x[i];
        yy += y[i];
    }
    double xbar = (1.0 * xx) / n, ybar = (1.0 * yy) / n;
    double sx = 0, sy = 0;
    for(int i = 1;i <= n;i++)
    {
        sx += (x[i] - xbar) * (x[i] - xbar);
        sy += (y[i] - ybar) * (y[i] - ybar);
    }
    sx /= n, sy /= n;
//     std::cout << sx << ' ' << sy << endl;
    if(std::abs(sx - ((200 * 200) / 12.0)) < 1000)
        std::cout << "bit-noob" << endl;
    else
        std::cout << "buaa-noob" << endl;
//     std::cout << sum << endl;
}

signed main()
{
#ifdef ONLINE_JUDGE
    ioclear;
#endif

    int t = 1;
//    std::cin >> t;
    while(t--)
        solve();
}