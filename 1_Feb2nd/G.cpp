//
// Created by Dusker on 2024/2/2.
//
#include <bits/stdc++.h>
#define ioclear std::ios::sync_with_stdio(false);std::cin.tie(nullptr);std::cout.tie(nullptr);
#define endl '\n'
#define int long long

void solve()
{
    using pii = std::pair<int, int>;
    using i64 = long long;
#define all(x) (x).begin(), (x).end()
    int n, m;
    std::cin >> n >> m;
    std::vector<pii> a(n);
    for(int i = 0;i < n;i++)
        std::cin >> a[i].first >> a[i].second;
    std::sort(all(a));
    for(int i = 1;i < n;i++)
        a[i].second += a[i - 1].second;
    for(int i = n - 1;i >= 0;i--)
        if(a[i].first - a[i].second <= m)
        {
            std::cout << m + a[i].second << endl;
            return;
        }
    std::cout << m << endl;
}

signed main()
{
#ifdef ONLINE_JUDGE
    ioclear;
#endif

    int t;
    std::cin >> t;
    while(t--)
        solve();
}