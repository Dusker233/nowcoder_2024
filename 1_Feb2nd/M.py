t = int(input())
for _ in range(t):
    n = int(input())
    if not n % 6:
        print(n // 6)
    else:
        print(n // 6 * 2)