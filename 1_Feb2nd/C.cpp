//
// Created by Dusker on 2024/2/2.
//
#include <bits/stdc++.h>
#define ioclear std::ios::sync_with_stdio(false);std::cin.tie(nullptr);std::cout.tie(nullptr);
#define endl '\n'
#define int long long

void solve()
{
    using pii = std::pair<int, int>;
    using i64 = long long;
#define all(x) (x).begin(), (x).end()
    int n, q, t, Smin = 0;
    std::cin >> n >> q >> t;
    std::vector<int> a(n), s(n + 1);
    for(int i = 0;i < n;i++)
        std::cin >> a[i];
    std::sort(all(a));
    for(int i = 1;i <= n;i++)
        s[i] = s[i - 1] + a[i - 1];
//      for(int i = 0;i <= n;i++)
//          std::cout << sc[i] << " \n"[i == n];
    while(q--)
    {
        int m;
        std::cin >> m;
        int cur = m / t;
        std::cout << s[std::max(0LL, n - cur)] + t << endl;
    }
}

signed main()
{
#ifdef ONLINE_JUDGE
    ioclear;
#endif

    int t = 1;
//    std::cin >> t;
    while(t--)
        solve();
}