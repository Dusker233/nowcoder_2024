#include <bits/stdc++.h>
#define ioclear std::ios::sync_with_stdio(false);std::cin.tie(nullptr);std::cout.tie(nullptr);
#define endl '\n'

void solve()
{
    using pii = std::pair<int, int>;
#define all(x) (x).begin(), (x).end()
    int n, m, ans = 0x3f3f3f3f;
    std::cin >> n >> m;
    std::vector<int> a(n);
    for(int i = 0;i < n;i++)
        std::cin >> a[i];
    std::vector<int> x(m), y(m);
    for(int i = 0;i < m;i++)
    {
        std::cin >> x[i] >> y[i];
        x[i]--;y[i]--;
    }
    auto dfs = [&](auto &&self, int cur) -> void
    {
        if(cur == m)
        {
            int rnk = 1;
            for(int j = 0;j < n;j++)
                rnk += (a[j] > a[0]);
            ans = std::min(ans, rnk);
            return;
        }
        for(auto [px, py]: {pii(3, 0), pii(1, 1), pii(0, 3)})
        {
            a[x[cur]] += px;
            a[y[cur]] += py;
            self(self, cur + 1);
            a[x[cur]] -= px;
            a[y[cur]] -= py;
        }
    };
    dfs(dfs, 0);
    std::cout << ans << endl;
}

int main()
{
#ifdef ONLINE_JUDGE
    ioclear;
#endif

    int t;
    std::cin >> t;
    while(t--)
        solve();
}