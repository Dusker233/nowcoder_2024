//
// Created by Dusker on 2024/2/2.
//
#include <bits/stdc++.h>
#define ioclear std::ios::sync_with_stdio(false);std::cin.tie(nullptr);std::cout.tie(nullptr);
#define endl '\n'
#define int long long

void solve()
{
    using pii = std::pair<int, int>;
    using i64 = long long;
#define all(x) (x).begin(), (x).end()
    int n, m;
    std::cin >> n >> m;
    std::vector<int> v(n), w(n);
    for(int i = 0;i < n;i++)
        std::cin >> v[i] >> w[i];
    int ans = 0;
    auto check = [&](int x)
    {
        int cur = 0;
        for(int i = 0;i < n;i++)
            if((x & w[i]) == w[i])
                cur += v[i];
        ans = std::max(ans, cur);
    };
    for(int i = 30;i >= 0;i--)
        if(m & (1 << i))
            check((m ^ (1 << i)) | ((1 << i) - 1));
    std::cout << ans << endl;
}

signed main()
{
#ifdef ONLINE_JUDGE
    ioclear;
#endif

    int t;
    std::cin >> t;
    while(t--)
        solve();
}