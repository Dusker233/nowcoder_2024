//
// Created by Dusker on 2024/2/5.
//
#include<bits/stdc++.h>

#define ioclear std::ios::sync_with_stdio(false);std::cin.tie(nullptr);std::cout.tie(nullptr);
#define endl '\n'
#define CLOCK 1e3 * clock() / CLOCKS_PER_SEC
#define int long long

#ifdef LOCAL
#define dmp(x) std::cerr << __LINE__ << " " << #x << " " << x << endl;
#else
#define dmp(x) void(0)
#endif

using pii = std::pair<int, int>;
constexpr int inf = 0x3f3f3f3f;

void solve()
{
    int n;
    std::cin >> n;
    std::vector<int> a(n);
    for(int i = 0;i < n;i++)
        std::cin >> a[i];
    std::sort(a.begin(), a.end());
    int ans = a[0] * (n - 1);
    for(int i = 1;i < n;i++)
    {
        int cur = std::min(a[i], 2 * a[0]);
        ans += cur * (n - i - 1);
    }
    std::cout << 4 * ans << endl;
}

signed main()
{
#ifdef ONLINE_JUDGE
    ioclear;
#endif

    int t = 1;
    std::cin >> t;
    while(t--)
        solve();

    std::cerr << "USED " << CLOCK << " ms";
}