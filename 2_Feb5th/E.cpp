//
// Created by Dusker on 2024/2/5.
//
#include<bits/stdc++.h>

#define ioclear std::ios::sync_with_stdio(false);std::cin.tie(nullptr);std::cout.tie(nullptr);
#define endl '\n'
#define CLOCK 1e3 * clock() / CLOCKS_PER_SEC

#ifdef LOCAL
#define dmp(x) std::cerr << __LINE__ << " " << #x << " " << x << endl;
#else
#define dmp(x) void(0)
#endif

using pii = std::pair<int, int>;
constexpr int inf = 0x3f3f3f3f;

void solve()
{
    int n;
    std::cin >> n;
    std::vector<int> col(n + 1);
    std::vector<std::vector<int>> pos(n + 1);
    for(int i = 1;i <= n;i++)
    {
        std::cin >> col[i];
        pos[col[i]].emplace_back(i);
    }
    int ans = 0;
    std::vector<int> mx(n + 1, inf);
    for(int i = 1;i <= std::min(n, 2);i++)
    {
        if(pos[i].empty())
            continue;
        mx[i] = *(std::max_element(pos[i].begin(), pos[i].end()));
        dmp(mx[i]);
    }

    int len = n;
    int mn = *(std::min_element(mx.begin() + 1, mx.end()));
    while(mn != inf)
    {
        for(int i = 1;i <= std::min(n, 2);i++)
        {
            if(pos[i].empty())
                continue;
            auto it = std::lower_bound(pos[i].begin(), pos[i].end(), mn);
            pos[i].erase(it, pos[i].end());
            mx[i] = pos[i].empty() ? inf : pos[i].back();
        }
        ans++;
        mn = *(std::min_element(mx.begin() + 1, mx.end()));
    }
    std::cout << ans << endl;
}

int main()
{
#ifdef ONLINE_JUDGE
    ioclear;
#endif

    int t = 1;
    std::cin >> t;
    while(t--)
        solve();

    std::cerr << "USED " << CLOCK << " ms";
}