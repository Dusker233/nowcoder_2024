//
// Created by Dusker on 2024/2/5.
//
#include<bits/stdc++.h>

#define ioclear std::ios::sync_with_stdio(false);std::cin.tie(nullptr);std::cout.tie(nullptr);
#define endl '\n'
#define CLOCK 1e3 * clock() / CLOCKS_PER_SEC

#ifdef LOCAL
#define dmp(x) std::cerr << __LINE__ << " " << #x << " " << x << endl;
#else
#define dmp(x) void(0)
#endif

using pii = std::pair<int, int>;

void solve()
{
    int n, m, k;
    std::cin >> n >> m >> k;
    std::vector mp(n + 1, std::vector(m + 1, 0));
    std::set<int> xe, ye;
    for(int i = 1;i <= k;i++)
    {
        int x, y;
        std::cin >> x >> y;
        xe.insert((x - 1) * m + y);
        xe.insert(m * x + y);
        ye.insert((y - 1) * n + x);
        ye.insert(n * y + x);
//         std::cout << (x - 1) * m + y << ' ' << (m * x + y) << ' ' << (y - 1) * n + x << ' ' << n * y + x << endl;
    }

    std::cout << xe.size() + ye.size() << endl;
}

int main()
{
#ifdef ONLINE_JUDGE
    ioclear;
#endif

    int t = 1;
//    std::cin >> t;
    while(t--)
        solve();

    std::cerr << "USED " << CLOCK << " ms";
}