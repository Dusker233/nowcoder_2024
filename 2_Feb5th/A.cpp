//
// Created by Dusker on 2024/2/5.
//
#include<bits/stdc++.h>

#define ioclear std::ios::sync_with_stdio(false);std::cin.tie(nullptr);std::cout.tie(nullptr);
#define endl '\n'
#define CLOCK 1e3 * clock() / CLOCKS_PER_SEC

#ifdef LOCAL
#define dmp(x) std::cerr << __LINE__ << " " << #x << " " << x << endl;
#else
#define dmp(x) void(0)
#endif

void solve()
{
    int a, b, c;
    std::cin >> a >> b >> c;
    int ans = (a - 100) / 50;
    auto check = [](int x) -> int
    {
        if (x == 34 || x == 36 || x == 38 || x == 40)
            return 1;
        else if (x == 45)
            return 2;
        else
            return 0;
    };
    ans += check(b) + check(c);
    std::cout << ans << endl;
}

int main()
{
#ifdef ONLINE_JUDGE
    ioclear;
#endif

    int t = 1;
    std::cin >> t;
    while (t--)
        solve();

    std::cerr << "USED " << CLOCK << " ms";
}