//
// Created by Dusker on 2024/2/13.
//
#include<bits/stdc++.h>

#define ioclear std::ios::sync_with_stdio(false);std::cin.tie(nullptr);std::cout.tie(nullptr);
#define endl '\n'
#define CLOCK 1e3 * clock() / CLOCKS_PER_SEC

#ifdef LOCAL
#define dmp(x) std::cerr << __LINE__ << " " << #x << " " << x << endl;
#else
#define dmp(x) void(0)
#endif

constexpr int mod = 1E9 + 7;

void solve()
{
    std::unordered_set<int> st;
    int n, y;
    std::string s;
    std::cin >> n >> s >> y;
    auto check = [&](std::vector<int> nums)
    {
        std::string cur;
        for(int i = 0;i < n;i++)
        {
            cur += s[i];
            if(s[i] >= 'a' && s[i] <= 'd')
                cur[i] = static_cast<char>(nums[s[i] - 'a'] + '0');
            if(s[i] == '_')
                cur[i] = static_cast<char>(nums[4] + '0');
        }
        if(cur[0] == '0' && n != 1)
            return;
        int x = std::stoi(cur);
        if((x % 8) || (x > y))
            return;
//         std::cout << x << endl;
        st.insert(x);
        return;
    };
    for(int a = 0; a < 10; a++)
        for(int b = 0; b < 10; b++)
            for(int c = 0; c < 10; c++)
                for(int d = 0; d < 10; d++)
                    for(int e = 0; e < 10; e++)
                    {
                        std::unordered_set<int> cur = {a, b, c, d};
                        if(cur.size() != 4)
                            continue;
                        check(std::vector<int>{a, b, c, d, e});
                    }
    std::cout << st.size() % mod << endl;
}

int main()
{
#ifdef ONLINE_JUDGE
    ioclear;
#endif

    int t = 1;
    std::cin >> t;
    while(t--)
        solve();

    std::cerr << "USED " << CLOCK << " ms";
}