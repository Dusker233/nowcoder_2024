//
// Created by Dusker on 2024/2/13.
//
#include<bits/stdc++.h>

#define ioclear std::ios::sync_with_stdio(false);std::cin.tie(nullptr);std::cout.tie(nullptr);
#define endl '\n'
#define CLOCK 1e3 * clock() / CLOCKS_PER_SEC

#ifdef LOCAL
#define dmp(x) std::cerr << __LINE__ << " " << #x << " " << x << endl;
#else
#define dmp(x) void(0)
#endif

using i64 = int64_t;
constexpr i64 inf = 0x3f3f3f3f3f3f3f3fll;
constexpr int mmax = 5E5 + 10;

template<class T>
struct Info
{
    T sum, rmx, lmn, lans, rans, lrans, ans;
    Info() = default;
    explicit Info(T x): sum(x), rmx(x), lmn(x), lans(-inf), rans(-inf), lrans(-inf), ans(-inf) {}
    friend std::ostream &operator <<(std::ostream &os, const Info &x)
    {
        return os << x.ans;
    }
};

template<class T>
Info<T> operator +(Info<T> x, Info<T> y)
{
    Info<T> res{};
    res.sum=x.sum + y.sum;
    res.rmx = std::max(y.rmx, x.rmx + y.sum);
    res.lmn = std::min(x.lmn, x.sum + y.lmn);
    res.lans = std::max({x.lans, x.sum + y.lans, x.sum - y.lmn, x.lrans - y.lmn});
    res.rans = std::max({y.rans, x.rans - y.sum, x.rmx - y.sum, x.rmx + y.lrans});
    res.lrans = std::max({x.lrans - y.sum, x.sum + y.lrans, x.sum - y.sum});
    res.ans = std::max({x.ans, y.ans, x.rmx - y.lmn, x.rans - y.lmn, x.rmx + y.lans});
    return res;
}

struct Tree
{
    int l, r;
    Info<i64> info;
} t[mmax << 2];

std::array<i64, mmax> a;

void pushup(int x) {t[x].info = t[x << 1].info + t[x << 1 | 1].info;}

void build(int l, int r, int p)
{
    t[p].l = l, t[p].r = r;
    if(l == r)
        return t[p].info = Info(a[l]), void();
    int mid = (l + r) >> 1;
    build(l, mid, p << 1);
    build(mid + 1, r, p << 1 | 1);
    pushup(p);
}

void update(int x, int v, int p)
{
    if(t[p].l == t[p].r)
        return t[p].info = Info(static_cast<i64>(v)), void();
    int mid = (t[p].l + t[p].r) >> 1;
    if(x <= mid)
        update(x, v, p << 1);
    else
        update( x, v, p << 1 | 1);
    pushup(p);
}

Info<i64> query(int l, int r, int p)
{
    if(t[p].l >= l && t[p].r <= r)
        return t[p].info;
    int mid = (t[p].l + t[p].r) >> 1;
    if(r <= mid)
        return query(l, r, p << 1);
    if(l > mid)
        return query(l, r, p << 1 | 1);
    return query(l, r, p << 1) + query(l, r, p << 1 | 1);
}

void solve()
{
    int n, q;
    std::cin >> n >> q;
    for(int i = 1;i <= n;i++)
        std::cin >> a[i];
    build(1, n, 1);
    while(q--)
    {
        int op, x, y;
        std::cin >> op >> x >> y;
        if(op == 1)
            update(x, y, 1);
        else
            std::cout << query(x, y, 1) << endl;
    }
}

int main()
{
#ifdef ONLINE_JUDGE
    ioclear;
#endif

    int t = 1;
    std::cin >> t;
    while(t--)
        solve();

    std::cerr << "USED " << CLOCK << " ms";
}