//
// Created by Dusker on 2024/2/13.
//
#include<bits/stdc++.h>
#define int long long

#define ioclear std::ios::sync_with_stdio(false);std::cin.tie(nullptr);std::cout.tie(nullptr);
#define endl '\n'
#define CLOCK 1e3 * clock() / CLOCKS_PER_SEC

#ifdef LOCAL
#define dmp(x) std::cerr << __LINE__ << " " << #x << " " << x << endl;
#else
#define dmp(x) void(0)
#endif

template<typename T>
struct Dijkstra
{
    static constexpr int mmax = 5e5 + 10;
    static constexpr T inf = 0x3f3f3f3f3f3f3f3fll;

    int n{}, m{};
    T dis[mmax]{};
    bool vis[mmax]{};
    struct node
    {
        int v;
        T w;
        node(int _v, T _w) : v(_v), w(_w) {}
        bool operator < (const node &x) const {return x.w < w;}
    };
    std::vector<node> edge[mmax];

    void dij(int x)
    {
        memset(dis, inf, sizeof dis);
        dis[x] = 0;
        std::priority_queue<node> q;
        q.push({x, 0});
        while(!q.empty())
        {
            node tmp = q.top();
            q.pop();
            if(vis[tmp.v]) continue;
            vis[tmp.v] = 1;
            for(auto t : edge[tmp.v])
            {
                if(dis[t.v] > dis[tmp.v] + t.w)
                {
                    dis[t.v] = dis[tmp.v] + t.w;
                    q.push({t.v, dis[t.v]});
                }
            }
        }
        return;
    }
    void add(int x, int y, T z)
    {
        edge[x].emplace_back(node(y, z));
        return;
    }
};

void solve()
{
    Dijkstra<int> dij;
    int n, m, k;
    std::cin >> n >> m >> k;
    dij.n = n;
    for(int i = 1;i <= m;i++)
    {
        int a, b;
        std::cin >> a >> b;
        for(int j = 0;j < n;j++)
            dij.add(j, (j + a) % n, b);
    }
    dij.dij(0);
//     for(int i = 0;i < n;i++)
//         std::cout << dij.dis[i] << " \n"[i == n - 1];
//     std::cout << n - k << ' ' << dij.dis[n - k] << endl;
    if(dij.dis[n - k] == dij.inf)
        std::cout << -1 << endl;
    else
        std::cout << dij.dis[n - k] << endl;
}

signed main()
{
#ifdef ONLINE_JUDGE
    ioclear;
#endif

    int t = 1;
    std::cin >> t;
    while(t--)
        solve();

    std::cerr << "USED " << CLOCK << " ms";
}