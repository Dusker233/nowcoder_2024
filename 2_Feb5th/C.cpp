//
// Created by Dusker on 2024/2/15.
//
#include<bits/stdc++.h>

#define ioclear std::ios::sync_with_stdio(false);std::cin.tie(nullptr);std::cout.tie(nullptr);
#define endl '\n'
#define CLOCK 1e3 * clock() / CLOCKS_PER_SEC

#ifdef LOCAL
#define dmp(x) std::cerr << __LINE__ << " " << #x << " " << x << endl;
#else
#define dmp(x) void(0)
#endif

constexpr int P = 1E9 + 7;
using i64 = long long;

// assume -P <= x < 2P
int norm(int x) {
    if (x < 0) {
        x += P;
    }
    if (x >= P) {
        x -= P;
    }
    return x;
}
template<class T>
T power(T a, i64 b) {
    T res = 1;
    for (; b; b /= 2, a *= a) {
        if (b % 2) {
            res *= a;
        }
    }
    return res;
}
struct Z {
    int x;
    Z(int x = 0) : x(norm(x)) {}
    Z(i64 x) : x(norm(x % P)) {}
    int val() const {
        return x;
    }
    Z operator-() const {
        return Z(norm(P - x));
    }
    Z inv() const {
        assert(x != 0);
        return power(*this, P - 2);
    }
    Z &operator*=(const Z &rhs) {
        x = i64(x) * rhs.x % P;
        return *this;
    }
    Z &operator+=(const Z &rhs) {
        x = norm(x + rhs.x);
        return *this;
    }
    Z &operator-=(const Z &rhs) {
        x = norm(x - rhs.x);
        return *this;
    }
    Z &operator/=(const Z &rhs) {
        return *this *= rhs.inv();
    }
    friend Z operator*(const Z &lhs, const Z &rhs) {
        Z res = lhs;
        res *= rhs;
        return res;
    }
    friend Z operator+(const Z &lhs, const Z &rhs) {
        Z res = lhs;
        res += rhs;
        return res;
    }
    friend Z operator-(const Z &lhs, const Z &rhs) {
        Z res = lhs;
        res -= rhs;
        return res;
    }
    friend Z operator/(const Z &lhs, const Z &rhs) {
        Z res = lhs;
        res /= rhs;
        return res;
    }
    friend std::istream &operator>>(std::istream &is, Z &a) {
        i64 v;
        is >> v;
        a = Z(v);
        return is;
    }
    friend std::ostream &operator<<(std::ostream &os, const Z &a) {
        return os << a.val();
    }
};

constexpr int mmax = 2E5 + 10;
constexpr int len = 30;

std::array<std::array<i64, 2>, mmax * len> trie;
std::array<Z, mmax * len> val;
i64 tot;

void insert(i64 x, Z v)
{
    i64 cur = 0;
    for(int i = len;i >= 0;i--)
    {
        i64 t = (x >> i) & 1;
        if(!(~trie[cur][t]))
        {
            trie[cur][t] = ++tot;
            val[tot] = Z(0);
            std::fill(trie[tot].begin(), trie[tot].end(), -1);
        }
        cur = trie[cur][t];
        val[cur] += v;
    }
}

Z query(i64 x, i64 k)
{
    i64 cur = 0;
    Z ans = 0;
    for(int i = len;i >= 0;i--)
    {
        if(cur == -1)
            return ans;
        i64 t = (x >> i) & 1;
        if(((k >> i) & 1) == 1)
        {
            if(~trie[cur][t])
                ans += val[trie[cur][t]];
            cur = trie[cur][t ^ 1];
        }
        else
            cur = trie[cur][t];
    }
    return ans + val[cur];
}

void solve()
{
    tot = 0;
    val[0] = Z(0);
    std::fill(trie[0].begin(), trie[0].end(), -1);
    int n, k;
    std::cin >> n >> k;
    std::vector<int> a(n + 1);
    for(int i = 1;i <= n;i++)
        std::cin >> a[i];
    std::sort(a.begin() + 1, a.end());
    Z ans = 1;
    for(int i = 2;i <= n;i++)
    {
        insert(a[i - 1], power(Z(2), i - 1).inv());
        ans = (ans + 1 + query(a[i], k) * power(Z(2), i - 1));
    }
    std::cout << ans << endl;

}

int main()
{
#ifdef ONLINE_JUDGE
    ioclear;
#endif

    int t = 1;
    std::cin >> t;
    while(t--)
        solve();

    std::cerr << "USED " << CLOCK << " ms";
}