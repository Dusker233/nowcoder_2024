//
// Created by Dusker on 2024/2/5.
//
#include<bits/stdc++.h>

#define ioclear std::ios::sync_with_stdio(false);std::cin.tie(nullptr);std::cout.tie(nullptr);
#define endl '\n'
#define CLOCK 1e3 * clock() / CLOCKS_PER_SEC

#ifdef LOCAL
#define dmp(x) std::cerr << __LINE__ << " " << #x << " " << x << endl;
#else
#define dmp(x) void(0)
#endif

using pii = std::pair<int, int>;
constexpr int inf = 0x3f3f3f3f;

void solve()
{
    int n, ans = 0, num = 0;
    std::cin >> n;
//    dmp(n);
    std::vector<int> col(n), cnt(n + 1);
    for(int i = 0;i < n;i++)
    {
        std::cin >> col[i];
        cnt[col[i]]++;
    }
    for(int i = 1;i <= n;i++)
        if(cnt[i])
            num++;
    std::set<int> s;
    for(int i = n - 1;i >= 0;i--)
    {
        s.insert(col[i]);
        if(s.size() == num)
        {
            ans++;
            s.clear();
        }
        if(--cnt[col[i]] == 0)
            num--, s.erase(col[i]);
    }
    std::cout << ans << endl;
}

int main()
{
#ifdef ONLINE_JUDGE
    ioclear;
#endif

//     freopen("E.txt", "r", stdin);
    int t = 1;
    std::cin >> t;
//    dmp(t);
    while(t--)
        solve();

    std::cerr << "USED " << CLOCK << " ms";
}